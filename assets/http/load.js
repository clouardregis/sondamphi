window.onload = () => {
    load_results()
}

function load_results() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        let data = JSON.parse(this.responseText);
        if (data.status !== 200) {
            document.getElementById("n_ans_a").textContent = "0";
            document.getElementById("p_ans_a").textContent = "0";
            document.getElementById("ans_a");

            document.getElementById("n_ans_b").textContent = "0";
            document.getElementById("p_ans_b").textContent = "0";
            document.getElementById("ans_b");

            document.getElementById("n_ans_c").textContent = "0";
            document.getElementById("p_ans_c").textContent = "0";
            document.getElementById("ans_c");

            document.getElementById("n_ans_d").textContent = "0";
            document.getElementById("p_ans_d").textContent = "0";
            document.getElementById("ans_d");
        } else {
            const totalVotes = data.data.A.num + data.data.B.num + data.data.C.num + data.data.D.num;
            let max_p = data.data.A.prop + data.data.B.prop + data.data.C.prop + data.data.D.prop
            document.getElementById("total").textContent =  totalVotes;
            if (totalVotes > 1) {
                document.getElementById("total_label").textContent =  "votes";
            } else {
                document.getElementById("total_label").textContent =  "vote";
            }
            document.getElementById("n_ans_a").textContent = data.data.A.num;
            document.getElementById("p_ans_a").textContent = `${Number.parseFloat((data.data.A.prop * 100).toFixed(1))}`;
            setProp(document.getElementById("ans_a"), data.data.A.prop / max_p);

            document.getElementById("n_ans_b").textContent = data.data.B.num;
            document.getElementById("p_ans_b").textContent = `${Number.parseFloat((data.data.B.prop * 100).toFixed(1))}`;
            setProp(document.getElementById("ans_b"), data.data.B.prop / max_p);

            document.getElementById("n_ans_c").textContent = data.data.C.num;
            document.getElementById("p_ans_c").textContent = `${Number.parseFloat((data.data.C.prop * 100).toFixed(1))}`;
            setProp(document.getElementById("ans_c"), data.data.C.prop / max_p);

            document.getElementById("n_ans_d").textContent = data.data.D.num;
            document.getElementById("p_ans_d").textContent = `${Number.parseFloat((data.data.D.prop * 100).toFixed(1))}`;
            setProp(document.getElementById("ans_d"), data.data.D.prop / max_p);
        }
    }
    xhttp.open("GET", "results.json", true);
    xhttp.send();
}

function setProp(el, prop) {
    el.style.setProperty("--prop", Math.max(0.01, prop) + "fr");
}