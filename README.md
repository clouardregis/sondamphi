# Contexte

VoxAmphi est un projet d'étudiants de la spécialité informatique de l'[ENSICAEN](https://www.ensicaen.fr).
Ce travail se base sur une reprise de l'application Android [VotAR](https://libre-innovation.org/index.fr.html) publiée sous licence publique générale [GNU Affero](https://www.gnu.org/licenses/agpl-3.0.html).
Cette application a été totalement réécrite en Flutter et publiée sous licence [MIT](https://opensource.org/license/mit/).

# Projet

Le projet consiste à développer une application mobile (Android et iOS) de sondage d’ambiance.
Dans un amphithéâtre, pour répondre à un sondage sur une question présentant jusqu’à quatre réponses,
chaque personne du public souhaitant voter présente un carton de vote conventionnel orienté selon la réponse choisie.

Le présentateur utilise l’application pour compter instantanément le nombre de votes pour chaque réponse possible à partir d’une photo de l’audience.

# Déroulé d'un vote 

Le présentateur de l'amphithéâtre distribue aux participants un [carton de vote conventionnel](etc/feuille_vote.pdf) sur lequel se trouve un damier de 4 cases, chacune avec une couleur différente.
Lorsque le présentateur veut effectuer un sondage, il énonce la question avec au maximum quatre réponses possibles. Les personnes qui souhaitent voter présentent devant eux le carton de vote en utilisant l'orientation pour exprimer leur choix. Le présentateur prend une photo de l'assemblée avec l’application. Les résultats s’affichent ensuite sur son smartphone en termes de nombre de votants et nombre de votes pour chaque choix.

# Affichage du résultat sur un ordinateur

L'application permet d'afficher le résultat d'un sondage dans le navigateur d'un ordinateur
et donc permet de vidéo-projecter le résultat du sondage.

Sur le téléphone qui contient l'application&nbsp;:

1. Créez un point d'accès WI-FI dans les paramètres du téléphone.
2. Lancez l'application, en bas de l'écran s'affiche l'adresse de connexion.

Sur l'ordinateur&nbsp;:

1. Connectez-vous sur le point d'accès WI-FI créé plus tôt sur le téléphone.
2. Ouvrez le navigateur Internet et tapez l'adresse de connexion relevé sur le téléphone.

# Respect de la vie privée

- Les cartons de votes sont tous identiques et il n'y a aucun moyen de remonter à la personne votante à partir d'un sondage.

- Même si le vote est basé sur l'analyse d'une photo prise de l'assemblée, l'image n'est jamais accessible à l'utilisateur.
Elle n'est ni affichée à l'écran ni stockée de façon permanente sous forme de fichier. L'image n'est gardée en mémoire que le temps du traitement.

- Il n'y a donc aucun moyen de vérifier que tous les votes ont correctement été pris en compte dans le résultat du sondage. N'utilisez donc cette application que pour des sondages purement ludiques.

# Code source

L’application est développée en Flutter.

## Traductions

Deux langues sont disponibles :
* Français
* English

## Licence

[Licence MIT](https://opensource.org/licenses/MIT)

## L'équipe ([ENSICAEN](https://www.ensicaen.fr/) - [spécialité informatique](https://www.ensicaen.fr/formations/ingenieur-informatique/))

- 2022-2023
    - Romain CAILLY
    - Elliot GAUDRON-PARRY
    - Jordy GELB
    - Nicolas RIBAULT

### Tuteur

- Régis CLOUARD
