import 'package:flutter_test/flutter_test.dart';
import 'package:voxamphi/model/http_server.dart';

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('Test stringification of HTTP response', () {
    expect(const HTTPResponse(200, payload: "my payload").toString(), "HTTP/1.0 200 OK\n\nmy payload");
  });

  test('Test HTTP request parsing', () {
    HTTPRequest req = HTTPRequest("GET /ressource.xxx HTTP/1.0\n"
        "header1: value1\n"
        "header2: value2");
    expect(req.resource, "/ressource.xxx");
    expect(req.scheme, "HTTP/1.0");
    expect(req.headers, {"header1": "value1", "header2": "value2"});
  });
}
