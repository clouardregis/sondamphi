import 'package:flutter_test/flutter_test.dart';
import 'package:voxamphi/model/image_data.dart';

void main() {
  // 3 column red, 3 column blue and 4 lines
  const List<int> pixelMatrixExample = [
    0x00FF0000,
    0x00FF0000,
    0x00FF0000,
    0x000000FF,
    0x000000FF,
    0x000000FF,
    0x00FF0000,
    0x00FF0000,
    0x00FF0000,
    0x000000FF,
    0x000000FF,
    0x000000FF,
    0x00FF0000,
    0x00FF0000,
    0x00FF0000,
    0x000000FF,
    0x000000FF,
    0x000000FF,
    0x00FF0000,
    0x00FF0000,
    0x00FF0000,
    0x000000FF,
    0x000000FF,
    0x000000FF,
  ];

  ImageData image = ImageData(6, 4, pixelMatrixExample);

  test("average33 test", () {
    List<int> outPixels = List.filled(36, 0);
    image.average3x3(pixelMatrixExample, outPixels);

    expect(outPixels[7], 0x00FF0000); // Red
    expect(outPixels[8], 0x00AA0055); // Average 6 red + 3 blue
    expect(outPixels[9], 0x005500AA); // Average 3 red + 6 blue
    expect(outPixels[10], 0x000000FF); // Blue
  });

  group('colorDistance test', () {
    test('returns 0 for identical colors', () {
      expect(image.colorDistance(0xFF0000, 0xFF0000), 0);
      expect(image.colorDistance(0x00FF00, 0x00FF00), 0);
      expect(image.colorDistance(0x0000FF, 0x0000FF), 0);
    });

    test('returns non-zero value for different colors', () {
      expect(image.colorDistance(0xFF0000, 0x00FF00), 130050);
      expect(image.colorDistance(0x00FF00, 0x0000FF), 130050);
      expect(image.colorDistance(0x0000FF, 0xFF0000), 130050);
    });
  });

  test('burnIfEdge sets alpha channel when color difference is above allowed delta', () {
    List<int> workPixels = List.from(pixelMatrixExample);
    int i = 3;
    int j = 1;
    image.burnEdgePixels_(pixelMatrixExample, workPixels, i, j);
    expect(workPixels[9], equals(0xFF0000FF));
  });

  test('isBurnedPixel return true on burned pixel', () {
    List<int> workPixels = List.from(pixelMatrixExample);
    workPixels[6] |= 0xFF000000;
    expect(image.isBurnedPixel(4, 2, workPixels), true);
  });

  test('checkSquare test', () {
    ImageData image = ImageData(3, 3, [
      0x0000FE00,
      0,
      0x00FEFE00,
      0,
      0,
      0,
      0x00FE00FF,
      0,
      0x0000FEFE,
    ]);
    // Test case 1
    int c1 = 0x0000EE00;
    int result1 = image.checkSquare(c1, 0);
    expect(result1, 0);
    // Test case 2
    int c2 = 0x00EEEE00;
    int result2 = image.checkSquare(c2, 1);
    expect(result2, 0);
    // Test case 3
    int c3 = 0x00EEEE;
    int result3 = image.checkSquare(c3, 2);
    expect(result3, 0);
    // Test case 4
    int c4 = 0xEE00EE;
    int result4 = image.checkSquare(c4, 3);
    expect(result4, 0);
  });

  group('findOnePattern test', () {
    test('should return 0 on pattern 0 found', () {
      List<int> inPixels2 = [0x0000FE00, 0, 0x00FEFE00, 0, 0, 0, 0x00FE00FF, 0, 0x0000FEFE];
      ImageData image3 = ImageData(3, 3, inPixels2);
      image3.pixelStepToCenter = 1;
      int result = image3.findOnePattern(inPixels2, 1, 1);
      expect(result, 0);
    });
    test('should return 1 on pattern 1 found', () {
      List<int> inPixels2 = [0x00FE00FF, 0, 0x0000FE00, 0, 0, 0, 0x0000FEFE, 0, 0x00FEFE00];
      ImageData image3 = ImageData(3, 3, inPixels2);

      image3.pixelStepToCenter = 1;
      int result = image3.findOnePattern(inPixels2, 1, 1);
      expect(result, 1);
    });
    test('should return 2 on pattern 2 found', () {
      List<int> inPixels2 = [0x0000FEFE, 0, 0x00FE00FF, 0, 0, 0, 0x00FEFE00, 0, 0x0000FE00];
      ImageData image3 = ImageData(3, 3, inPixels2);

      image3.pixelStepToCenter = 1;
      int result = image3.findOnePattern(inPixels2, 1, 1);
      expect(result, 2);
    });

    test('should return 3 on pattern 3 found', () {
      List<int> inPixels2 = [0x00FEFE00, 0, 0x0000FEFE, 0, 0, 0, 0x0000FE00, 0, 0x00FE00FF];
      ImageData image3 = ImageData(3, 3, inPixels2);

      image3.pixelStepToCenter = 1;
      int result = image3.findOnePattern(inPixels2, 1, 1);
      expect(result, 3);
    });
    test('should return -1 on no pattern found', () {
      List<int> inPixels2 = [0x00FEFE00, 0, 0, 0, 0, 0, 0, 0, 0x00FE00FF];
      ImageData image3 = ImageData(3, 3, inPixels2);

      image3.pixelStepToCenter = 1;
      int result = image3.findOnePattern(inPixels2, 1, 1);
      expect(result, -1);
    });
  });

  group('findAllPattern test', () {
    test('should return 1 in each mark of res and markCount should be 4', () {
      List<int> inPixels = [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0,
        0,
        0,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0,
        0,
        0,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0,
        0,
        0,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0,
        0,
        0x0000FEFE,
        0x0000FEFE,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0,
        0,
        0,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0x00FE00FF,
        0x00FE00FF,
        0,
        0,
        0,
        0x00FEFE00,
        0x00FEFE00,
        0,
        0x0000FE00,
        0x0000FE00,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ];
      List<int> workPixels = List.from(inPixels);
      ImageData img = ImageData(18, 17, inPixels);
      img.burnRadius = 4;
      img.pixelStepToCenter = 1;
      List<int> votes = img.findAllPatterns(inPixels, workPixels);
      expect(votes, [1, 1, 1, 1]);
    });

    group('test markPixels', () {
      test("Color and OverSize test: should be 255 everywhere", () {
        List<int> inPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        ImageData image = ImageData(3, 3, inPixels);
        image.markPixels(inPixels, 1, 1, 0x000000FF, 10);
        List<int> expectedPixels = [255, 255, 255, 255, 255, 255, 255, 255, 255];
        expect(inPixels, expectedPixels);
      });

      test("Normal Size test: should be 255 on the top left square", () {
        List<int> inPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        ImageData image = ImageData(3, 3, inPixels);
        image.markPixels(inPixels, 1, 1, 0x000000FF, 1);
        List<int> expectedPixels = [255, 255, 0, 255, 255, 0, 0, 0, 0];
        expect(inPixels, expectedPixels);
      });

      test("0 size test: no change should be found", () {
        List<int> inPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        ImageData image = ImageData(3, 3, inPixels);
        image.markPixels(inPixels, 1, 1, 0x000000FF, 0);
        List<int> expectedPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        expect(inPixels, expectedPixels);
      });

      test("Out of bound x and y: no change should be found", () {
        List<int> inPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        ImageData image = ImageData(3, 3, inPixels);
        image.markPixels(inPixels, 5, 5, 0x000000FF, 1);
        List<int> expectedPixels = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        expect(inPixels, expectedPixels);
      });
    });
  });
}
