import 'package:flutter_test/flutter_test.dart';
import 'package:voxamphi/model/survey.dart';

void main() {
  test("test survey empty case", () {
    List<int> results = [0, 0, 0, 0];
    Survey survey = Survey(results);
    expect(survey.voteCount(0), 0);
    expect(survey.voteCount(1), 0);
    expect(survey.voteCount(2), 0);
    expect(survey.voteCount(3), 0);
    expect(survey.percent(0), 0);
    expect(survey.percent(1), 0);
    expect(survey.percent(2), 0);
    expect(survey.percent(3), 0);
    expect(survey.totalVotes, 0);
  });

  test("test survey one case", () {
    List<int> results = [0, 1, 0, 0];
    Survey survey = Survey(results);
    expect(survey.voteCount(0), 0);
    expect(survey.voteCount(1), 1);
    expect(survey.voteCount(2), 0);
    expect(survey.voteCount(3), 0);
    expect(survey.percent(0), 0);
    expect(survey.percent(1), 1);
    expect(survey.percent(2), 0);
    expect(survey.percent(3), 0);
    expect(survey.totalVotes, 1);
  });

  test("test survey nominal case", () {
    List<int> results = [0, 35, 50, 15];
    Survey survey = Survey(results);
    expect(survey.voteCount(0), 0);
    expect(survey.voteCount(1), 35);
    expect(survey.voteCount(2), 50);
    expect(survey.voteCount(3), 15);
    expect(survey.percent(0), 0);
    expect(survey.percent(1), 0.35);
    expect(survey.percent(2), 0.50);
    expect(survey.percent(3), 0.15);
    expect(survey.totalVotes, 100);
  });
}
