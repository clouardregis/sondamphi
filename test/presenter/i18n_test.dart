import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:voxamphi/view/i18n.dart';

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('Test existence of translation file for all supported languages', () async {
    for (final locale in supportedLocales) {
      I18n.load(Locale(locale));
      expect(I18n.currentLanguage, locale);
    }
  });

  test('Test formatNumber when 0, 1, 1000, 1000.14 and locale is French', () async {
    I18n.load(const Locale("fr", "FR"));
    expect(I18n.formatNumber(0), '0');
    expect(I18n.formatNumber(1), '1');
    expect(I18n.formatNumber(1000), '1 000');
    expect(I18n.formatNumber(0.11), '0,11');
    expect(I18n.formatNumber(1000.10), '1 000,1');
  });

  test('Test existence buildListWithSupportedLocales', () async {
    expect(
      buildListWithSupportedLocales_(['en', 'fr', 'it']),
      [
        const Locale('en', ''),
        const Locale('fr', ''),
        const Locale('it', ''),
      ],
    );
  });
}
