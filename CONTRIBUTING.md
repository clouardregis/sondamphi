# Contribuer au développement de l'application mobile

## Licence

Le projet est placé sous la [licence MIT](https://opensource.org/licenses/MIT).

## Installer la distribution

Le projet est développé avec le kit de développement Flutter.
L'ensemble des codes sources de VoxAmphi est accessible sur le GitLab du projet : [https://gitlab.com/clouardregis/voxamphi](https://gitlab.com/clouardregis/voxamphi).

## Compiler la distribution

Avant de compiler, il est nécessaire de procéder à l'installation des bibliothèques incluses dans le projet.
Elles sont référencées dans le fichier `/pubspec.yaml`.

L'importation se fait par la commande Shell :
```
flutter pub get
```

- Le splashscreen : flutter_native_splash

L'installation se fait par la commande Shell :

```
flutter pub run flutter_native_splash:create
```

La construction de l'archive finale :

```
flutter build appbundle --release --obfuscate --split-debug-info=./debug/
```

Si nécessaire, la construction des icônes d'application pour Android et iOS se fait 
```
flutter pub run flutter_launcher_icons:main
```
