import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'view/i18n.dart';
import 'presenter/presenter.dart';
import 'view/home.dart';
import 'view/splash_screen.dart';
import 'view/theme.dart';

Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();
  var presenter = Presenter();
  presenter.startServer();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  runApp(VikazimutApplication(presenter));
}

final GlobalKey<NavigatorState> globalKeyContext = GlobalKey<NavigatorState>();

class VikazimutApplication extends StatelessWidget {
  final Presenter presenter;

  const VikazimutApplication(this.presenter, {super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: globalKeyContext,
      supportedLocales: buildListWithSupportedLocales(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        TranslationsDelegate(),
      ],
      debugShowCheckedModeBanner: false,
      theme: ApplicationTheme.darkTheme,
      home: Scaffold(
        body: SplashScreen(
          home: Home(presenter: presenter),
        ),
      ),
    );
  }
}
