import 'dart:ui';

class Survey {
  static const int maxVoteCount = 512;
  int _totalVotes = 0;
  final List<int> _votes = [0, 0, 0, 0];
  final List<double> _percents = [0, 0, 0, 0];
  final List<SurveyFeature> features = [
    SurveyFeature("A", const Color.fromRGBO(255, 14, 58, 1)),
    SurveyFeature("B", const Color.fromARGB(255, 76, 127 , 245)),
    SurveyFeature("C", const Color.fromARGB(255, 45, 180, 72)),
    SurveyFeature("D", const Color.fromARGB(255, 255, 183, 52)),
  ];

  Survey(List<int> result) {
    _totalVotes = 0;
    for (var i = 0; i < result.length; ++i) {
      _totalVotes += result[i];
    }
    for (var i = 0; i < result.length; ++i) {
      _votes[i] = result[i];
      if (totalVotes != 0) {
        _percents[i] = _votes[i] / totalVotes;
      } else {
        _percents[i] = 0;
      }
    }
  }

  int get totalVotes => _totalVotes;

  int get totalResponses => 4;

  int voteCount(int i) => _votes[i];

  double percent(int i) => _percents[i];

  Map<String, Map<String, double>> get asSerializable {
    var data = <String, Map<String, double>>{};
    for (var i = 0; i < totalResponses; ++i) {
      data[features[i].name] = {"num": _votes[i].toDouble(), "prop": _percents[i].toDouble()};
    }
    return data;
  }
}

class SurveyFeature {
  final String name;
  final Color color;

  SurveyFeature(this.name, this.color);
}
