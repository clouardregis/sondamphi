import 'dart:isolate';
import 'dart:math';
import 'package:flutter/foundation.dart';

import 'survey.dart';

class ImageData {
  static const int _averageDifferenceAllowed = 184;
  static const int _colorDiffStep = 1;
  static const int _colorDiffAllowedDelta = 0x24 * 0x24;
  static const int _hueDifferenceLinearMultiplier = 0x03;
  static const int _saturationNominator = 0x400;
  static const int _saturationVOffset = 0x05;
  static const int _saturationHOffset = 0x01;

  // Pixel encoding
  // |   1    |   2     |   3     |...
  // [R G B A | R G B A | R G B A |...]
  final List<int> _pixelsRGBA;
  final int _width;
  final int _height;
  late int _burnRadius;
  late int _pixelStepToCenter;

  SendPort? _sendPort;

  ImageData(this._width, this._height, this._pixelsRGBA) {
    int maxDim = max(_width, _height);
    _burnRadius = 8 + maxDim ~/ 256;
    _pixelStepToCenter = 2 + maxDim ~/ 512;
  }

  @visibleForTesting
  set burnRadius(int value) => _burnRadius = value;

  @visibleForTesting
  set pixelStepToCenter(int value) => _pixelStepToCenter = value;

  List<int> simpleAnalyze(SendPort sendPort) {
    _sendPort = sendPort;
    List<int> workPixels = _preprocessImage();
    var x = findAllPatterns(_pixelsRGBA, workPixels);
    _sendPort?.send(0.95);
    return x;
  }

  List<int> _preprocessImage() {
    List<int> workPixels = List<int>.filled(_width * _height, 0);
    average3x3(_pixelsRGBA, workPixels);
    _sendPort?.send(0.7);
    return workPixels;
  }

  @visibleForTesting
  void average3x3(List<int> inPixels, List<int> outPixels) {
    for (int y = 1; y < _height - 1; y++) {
      _sendPort?.send(0.5 + (0.2 * y) / _height);
      for (int x = 1; x < _width - 1; x++) {
        final i = x + y * _width;
        final pixel0 = inPixels[i - 1 - _width];
        final pixel1 = inPixels[i - _width];
        final pixel2 = inPixels[i + 1 - _width];
        final pixel3 = inPixels[i - 1];
        final pixel4 = inPixels[i];
        final pixel5 = inPixels[i + 1];
        final pixel6 = inPixels[i - 1 + _width];
        final pixel7 = inPixels[i + _width];
        final pixel8 = inPixels[i + 1 + _width];

        outPixels[i] = (((((pixel0 & 0x00FF0000) +
                            (pixel1 & 0x00FF0000) +
                            (pixel2 & 0x00FF0000) // Row -1, component 1
                            +
                            (pixel3 & 0x00FF0000) +
                            (pixel4 & 0x00FF0000) +
                            (pixel5 & 0x00FF0000) // Row  0, component 1
                            +
                            (pixel6 & 0x00FF0000) +
                            (pixel7 & 0x00FF0000) +
                            (pixel8 & 0x00FF0000)) // Row +1, component 1 RED
                        ~/
                        9) &
                    0x00FF0000) |
                ((((pixel0 & 0x0000FF00) + //GREEN
                            (pixel1 & 0x0000FF00) +
                            (pixel2 & 0x0000FF00) // Row -1, component 2
                            +
                            (pixel3 & 0x0000FF00) +
                            (pixel4 & 0x0000FF00) +
                            (pixel5 & 0x0000FF00) // Row  0, component 2
                            +
                            (pixel6 & 0x0000FF00) +
                            (pixel7 & 0x0000FF00) +
                            (pixel8 & 0x0000FF00)) // Row +1, component 2
                        ~/
                        9) &
                    0x0000FF00) |
                ((((pixel0 & 0x000000FF) + //BLUE
                            (pixel1 & 0x000000FF) +
                            (pixel2 & 0x000000FF) // Row -1, component 3
                            +
                            (pixel3 & 0x000000FF) +
                            (pixel4 & 0x000000FF) +
                            (pixel5 & 0x000000FF) // Row  0, component 3
                            +
                            (pixel6 & 0x000000FF) +
                            (pixel7 & 0x000000FF) +
                            (pixel8 & 0x000000FF)) // Row +1, component 3
                        ~/
                        9) &
                    0x000000FF)) &
            0x00FFFFFF;
      }
    }
  }

  @visibleForTesting
  List<int> findAllPatterns(List<int> inPixels, List<int> workPixels) {
    int totalVotes = 0;
    List<int> votes = [0, 0, 0, 0];
    final int startingX = _pixelStepToCenter + max(1, _colorDiffStep);
    final int startingY = _pixelStepToCenter + max(1, _colorDiffStep);
    final int endingX = _width - _pixelStepToCenter - max(1, _colorDiffStep);
    final int endingY = _height - _pixelStepToCenter - max(1, _colorDiffStep);

    // basic edge detection to burn pixels on an edge
    for (int y = startingY; y < endingY; y += 2) {
      for (int x = startingX; x < endingX; x += 2) {
        burnEdgePixels_(inPixels, workPixels, x, y);
      }
    }

    for (int y = startingY; y < endingY; y += 2) {
      _sendPort?.send(0.7 + ((0.3 * y) / (endingY - startingY)));
      for (int x = startingX; x < endingX; x += 2) {
        // skip burned pixels
        if (isBurnedPixel(x, y, workPixels)) {
          continue;
        }
        int patternIndex = findOnePattern(workPixels, x, y);
        if (patternIndex >= 0) {
          votes[patternIndex]++;
          // Burn the workPixels to make sure we do not count the same square 2 times
          markPixels(workPixels, x, y, 0xFF000000, _burnRadius);
          totalVotes++;
          // Why this limit?
          if (totalVotes >= Survey.maxVoteCount) {
            return votes;
          }
        }
      }
    }
    return votes;
  }

  @visibleForTesting
  bool isBurnedPixel(final int x, final int y, List<int> workPixels) {
    return ((workPixels[(x - _pixelStepToCenter) + _width * (y - _pixelStepToCenter)] & 0xFF000000) != 0 || (workPixels[(x + _pixelStepToCenter) + _width * (y - _pixelStepToCenter)] & 0xFF000000) != 0 || (workPixels[(x + _pixelStepToCenter) + _width * (y + _pixelStepToCenter)] & 0xFF000000) != 0 || (workPixels[(x - _pixelStepToCenter) + _width * (y + _pixelStepToCenter)] & 0xFF000000) != 0);
  }

  // compute squared color distance from color vector
  // we compare 2 pixels this way for edge detection
  @visibleForTesting
  int colorDistance(int c1, int c2) {
    int r1 = (c1 & (0x000000FF)), g1 = ((c1 >> 8) & 0x000000FF), b1 = ((c1 >> 16) & 0x000000FF);
    int r2 = (c2 & 0x000000FF), g2 = ((c2 >> 8) & 0x000000FF), b2 = ((c2 >> 16) & 0x000000FF);
    return (r1 - r2) * (r1 - r2) + (g1 - g2) * (g1 - g2) + (b1 - b2) * (b1 - b2);
  }

  @visibleForTesting
  void burnEdgePixels_(List<int> inPixels, List<int> workPixels, final int x, final int y) {
    // Use discrete [1, -1] mask to detect edge in column and row
    if (colorDistance(workPixels[x - _colorDiffStep + y * _width], workPixels[x + _colorDiffStep + y * _width]) > _colorDiffAllowedDelta || colorDistance(workPixels[x + (y - _colorDiffStep) * _width], workPixels[x + (y + _colorDiffStep) * _width]) > _colorDiffAllowedDelta) {
      workPixels[x + y * _width] |= 0xFF000000;
    }
  }

  @visibleForTesting
  int findOnePattern(List<int> workPixels, int x, int y) {
    // Given a particular pixel position on the photo, we check if it match a pattern rotation
    // return the pattern index, or -1 if it does not match
    // Bruteforce every square for every rotation, not optimal but adequate
    final int center = x + _width * y;
    final int hStep = _pixelStepToCenter;
    final int vStep = _width * hStep;

    // We use x/y as the center of this pattern, each table cell match a sub-square
    //  0 | 1
    // ---X---
    //  3 | 2
    final List<int> crossPixels = [
      workPixels[center - vStep - hStep],
      workPixels[center - vStep + hStep],
      workPixels[center + vStep + hStep],
      workPixels[center + vStep - hStep],
    ];

    // We loop on the 4 possible rotations of the first pattern
    // rotation=0  rotation=1    rotation=2  rotation=3
    //  0 | 1        1 | 2        2 | 3         3 | 0
    // -------  ->  -------  ->  --------  ->  -------
    //  3 | 2        0 | 3        1 | 0         2 | 1
    for (int rotation = 0; rotation < 4; rotation++) {
      int diff = 0;
      // for each sub-square of this pattern rotation, add up the difference with the reference color
      for (int i = 0; i < 4; i++) {
        // Check every square
        // Colors : Green, Yellow, Cyan, Magenta
        // pr=0
        //  G |            | C          |             |
        // -------  ->  -------  ->  --------  ->  -------
        //    |            |            | Y         M |
        //
        // pr=1
        //  M |            | G          |             |
        // -------  ->  -------  ->  --------  ->  -------
        //    |            |            | C         Y |
        //
        // ...
        diff += checkSquare(crossPixels[(rotation + i) % 4], i);
      }
      if (diff <= _averageDifferenceAllowed) {
        return rotation;
      }
    }
    return -1;
  }

  @visibleForTesting
  int checkSquare(int pixel, int colorIndex) {
    // colorIndex = reference colors components clockwise
    //		  R    G    B
    //		{0x00,0xFF,0x00},		0 -> green
    //		{0xFF,0xFF,0x00},		1 -> yellow
    //		{0x00,0xFF,0xFF},		2 -> cyan
    //		{0xFF,0x00,0xFF},		3 -> magenta

    // maths : https://www.desmos.com/calculator/b0g1wkqyju

    // color components
    int r = (pixel & 0x000000FF);
    int g = ((pixel >> 8) & 0x000000FF);
    int b = ((pixel >> 16) & 0x000000FF);

    // total match value accounting for hue difference and saturation
    int totalDifference = 0;
    // relative saturation, allow negative values (if we have the opposite hue)
    int saturation = 0;
    // arbitrary value of a hue difference
    int hueDifference = 0;

    switch (colorIndex) {
      case 2: // Yellow,
        // r and  g are strong, b is weak

        // first, outcast anything where b is not the weakest color
        // which means dead square (completely wrong hue) and we stop here
        //
        // to check saturation, we calculate stronger - weaker (and get an absolute value 0 <= sat <= 0xFF
        //
        // to check for more moderate hue difference
        // 0     n      n'    255
        // .. b .... g .... r ..
        //    r-g/g-b/sat        (with r-b being saturation)
        //
        //     or
        // .. b .... r .... g ..
        //
        // what we want with hue check is : are g and r close together or one of them is close to b ?
        // raw hueDifference = 0 : very close
        if (b >= r || b >= g) return 0x400;

        // we assume one of the strong component > weak to calculate a relative saturation
        // if it's not the case, we have a "negative" saturation, which is mean it has a completely reversed hue
        if (r > g) {
          saturation = r - b;
          hueDifference = ((r - g) * _hueDifferenceLinearMultiplier) ~/ (g - b + 1);
        } else {
          saturation = g - b;
          hueDifference = ((g - r) * _hueDifferenceLinearMultiplier) ~/ (r - b + 1);
        }
        hueDifference = (hueDifference * 0x100) ~/ saturation;

        break;
      case 1: // Cyan,
        // g and b are strong, r is weak
        if (r >= g || r >= b) return 0x400;

        if (g > b) {
          saturation = g - r;
          hueDifference = ((g - b) * _hueDifferenceLinearMultiplier) ~/ (b - r);
        } else {
          saturation = b - r;
          hueDifference = ((b - g) * _hueDifferenceLinearMultiplier) ~/ (g - r);
        }
        hueDifference = (hueDifference * 0x100) ~/ saturation;

        break;
      case 3: // Magenta,
        // r and b are strong, g is weak
        if (g >= r || g >= b) return 0x400;

        if (r > b) {
          saturation = r - g;
          hueDifference = ((r - b) * _hueDifferenceLinearMultiplier) ~/ (b - g);
        } else {
          saturation = b - g;
          hueDifference = ((b - r) * _hueDifferenceLinearMultiplier) ~/ (r - g);
        }
        hueDifference = (hueDifference * 0x100) ~/ saturation;

        break;
      case 0: // Green,
        // r and b are weak, g is strong
        if (g <= r || g <= b) return 0x400;

        if (r > b) {
          saturation = g - b;
          hueDifference = ((r - b) * _hueDifferenceLinearMultiplier) ~/ (g - r);
        } else {
          saturation = g - r;
          hueDifference = ((b - r) * _hueDifferenceLinearMultiplier) ~/ (g - b);
        }
        hueDifference = (hueDifference * 0x100) ~/ saturation;
        break;
    }
    totalDifference += hueDifference;

    // adjusted 1/sat curve :
    // extremely low saturation = dead square
    // very low sat = penalize much
    // low, medium sat = penalize very little (to still work in dark rooms)
    // high sat = don't penalize, might give a small bonus
    if (saturation >= 0) {
      // a quite soft exponential curve for low values : x * x / 0x80
      totalDifference += (_saturationNominator / (saturation + _saturationHOffset) - _saturationVOffset).truncate();
    } else {
      totalDifference += _averageDifferenceAllowed * 2;
    }
    return totalDifference;
  }

  @visibleForTesting
  void markPixels(List<int> pixels, int x, int y, int color, int size) {
    final int jStart = max(y - size, 0);
    final int jEnd = min(y + size, _height);
    final int iStart = max(x - size, 0);
    final int iEnd = min(x + size, _width);

    for (int j = jStart; j < jEnd; j++) {
      for (int i = iStart; i < iEnd; i++) {
        pixels[i + j * _width] = color;
      }
    }
  }
}
