import 'dart:core';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:path/path.dart' as path;

const String assetResourceDir = "assets/http";
const payload403 = """
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
  <head>
   <title>403 Forbidden</title>
  </head>
  <body>
   <h1>Forbidden</h1>
   <p>You don't have permission to access this resource.</p>
  </body>
</html>
""";
const payload404 = """
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
  <head>
    <title>404 Not Found</title>
  </head><body>
   <h1>Not Found</h1>
   <p>The requested URL was not found on this server.</p>
   </body>
</html>
""";

typedef HTTPHandler = Future<HTTPResponse> Function(HTTPRequest request);

class HTTPRequest {
  late bool isGetMethod;
  late String resource;
  late String scheme;
  late Map<String, String> headers;

  HTTPRequest(String stringRequest) {
    var req = stringRequest.split("\n\n");
    var header = req[0].split("\n");
    var header_0 = header[0].split(" ");

    isGetMethod = header_0[0] == "GET";
    resource = header_0[1];
    scheme = header_0[2];
    headers = {};
    header.sublist(1).forEach((h) {
      var hl = h.split(': ');
      if (hl.length > 1) {
        headers[hl[0]] = hl[1];
      }
    });
  }
}

class HTTPResponse {
  final int code;
  final String? payload;
  static const String scheme = "HTTP/1.0";

  const HTTPResponse(this.code, {this.payload});

  @override
  String toString() {
    String msg = {200: "OK", 403: "Forbidden", 404: "Not Found", 501: "Not Implemented"}[code] ?? "";
    String payload_ = payload ?? {403: payload403, 404: payload404}[code] ?? "";
    return "$scheme $code $msg\n\n$payload_";
  }
}

class VoxHTTPServer {
  final int _port;
  late String jsonResults;
  ServerSocket? _socket;
  final Map<String, HTTPHandler> _getHandlers = {};

  VoxHTTPServer(this._port) {
    _initialize();
  }

  void _initialize() {
    _onGet("/results.json", (request) async => HTTPResponse(200, payload: jsonResults));
  }

  Future<void> run() async {
    try {
      _socket = await ServerSocket.bind(InternetAddress.anyIPv4, _port);
      _socket?.listen(_handleConnection);
    } catch (_) {}
  }

  Future<String> getIpServer() async {
    final info = NetworkInfo();
    final wifiIP = await info.getWifiIP();
    if (wifiIP != null) {
      return "$wifiIP:$_port";
    } else {
      return Future.error("ip_server_network_error");
    }
  }

  void _handleConnection(Socket client) {
    client.listen(
      (Uint8List data) async {
        final request = HTTPRequest(String.fromCharCodes(data));
        var handler = _getHandler(request);
        client.write(await handler(request));
        client.close();
      },
      onError: (error) {
        client.close();
      },
      onDone: () {
        client.close();
      },
    );
  }

  HTTPHandler _getHandler(HTTPRequest request) {
    if (request.isGetMethod) {
      return _getHandlers[request.resource] ?? _defaultGetHandler;
    } else {
      return (HTTPRequest _) async {
        return const HTTPResponse(501);
      };
    }
  }

  Future<HTTPResponse> _defaultGetHandler(HTTPRequest request) async {
    var pathToResource = path.join("$assetResourceDir/", request.resource.substring(1));
    if (pathToResource.endsWith('/')) {
      pathToResource = path.join(pathToResource, "index.html");
    }
    var payload = await _openResource(pathToResource);
    // make sure client isn't requesting a file it shouldn't access
    if (!path.isWithin(assetResourceDir, pathToResource)) {
      return _failForbidden();
    } else if (payload == null) {
      return _failNotFound();
    }
    return HTTPResponse(200, payload: payload);
  }

  HTTPResponse _failForbidden() => const HTTPResponse(403);

  HTTPResponse _failNotFound() => const HTTPResponse(404);

  void _onGet(String resourceLocation, HTTPHandler handler) {
    _getHandlers[resourceLocation] = handler;
  }

  Future<String?> _openResource(String filename) async {
    try {
      return await rootBundle.loadString(filename);
    } catch (_) {
      return null;
    }
  }
}
