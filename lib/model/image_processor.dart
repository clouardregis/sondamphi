import 'dart:isolate';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:image/image.dart' as image_lib;

import 'image_data.dart';
import 'survey.dart';

class ImageProcessor {
  final XFile imageFile;
  final SendPort sendPort;

  ImageProcessor({required this.imageFile, required this.sendPort});

  Future<Survey> analyse() async {
    sendPort.send(0.005);
    ImageData imageData = await _convertXFileToImageData(imageFile);
    List<int> votes = imageData.simpleAnalyze(sendPort);
    var survey = Survey(votes);
    sendPort.send(1.0);
    return survey;
  }

  List<int> _convertToImageData(int width, int height, image_lib.Image image) {
    List<int> pixels = List<int>.filled(width * height, 0);
    int index = 0;
    for (final pixel in image) {
      pixels[index++] = (pixel.b.toInt() << 16) | (pixel.g.toInt() << 8) | pixel.r.toInt();
    }
    return pixels;
  }

  Future<ImageData> _convertXFileToImageData(XFile file) async {
    Uint8List bytes = await file.readAsBytes();
    sendPort.send(0.05);
    image_lib.Image? img = image_lib.decodeNamedImage(file.path, bytes);
    if (img == null) {
      throw "Internal error: cannot decode the image";
    }
    sendPort.send(0.35);
    List<int> pixels = _convertToImageData(img.width, img.height, img);
    sendPort.send(0.5);
    return ImageData(img.width, img.height, pixels);
  }
}
