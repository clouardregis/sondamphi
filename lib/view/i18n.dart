import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';
import 'package:voxamphi/main.dart';

const List<String> supportedLocales = ['en', 'fr'];

List<Locale> buildListWithSupportedLocales() {
  return buildListWithSupportedLocales_(supportedLocales);
}

@visibleForTesting
List<Locale> buildListWithSupportedLocales_(List<String> localesAsStrings) {
  return localesAsStrings.map((item) => Locale(item)).toList();
}

class I18n {
  static Locale _locale = const Locale('en');
  static late Map<dynamic, dynamic> _localizedValues;

  I18n._();

  static Locale get locale => _locale;

  static I18n of(BuildContext context) {
    return Localizations.of<I18n>(context, I18n)!;
  }

  static Future<I18n> load(Locale locale) async {
    _locale = locale;
    I18n translations = I18n._();
    String jsonContent = await rootBundle.loadString("i18n/${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);
    return translations;
  }

  static get currentLanguage => _locale.languageCode;

  static String getString(String key, [var parameter]) {
    if (globalKeyContext.currentContext == null) {
      return key;
    }
    return I18n.of(globalKeyContext.currentContext!)._getString(key, parameter);
  }

  String _getString(String key, [var parameter]) {
    if (_localizedValues[key] == null) {
      return '** $key not found';
    }
    if (parameter != null) {
      if (parameter is List) {
        return sprintf(_localizedValues[key], parameter);
      } else {
        return sprintf(_localizedValues[key], [parameter]);
      }
    } else {
      return _localizedValues[key];
    }
  }

  static String formatNumber(num length) {
    NumberFormat formatter = NumberFormat('#,##0.##', _locale.toString());
    return (formatter.format(length));
  }
}

class TranslationsDelegate extends LocalizationsDelegate<I18n> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) => supportedLocales.contains(locale.languageCode);

  @override
  Future<I18n> load(Locale locale) => I18n.load(locale);

  @override
  bool shouldReload(TranslationsDelegate old) => false;
}
