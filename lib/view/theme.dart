import 'package:flutter/material.dart';

const Color kSecondaryColorLight = Color(0xFFFF88FF);

const Color kPrimaryColor = Color(0xFF781BC0);
const Color kPrimaryColorLight = Color(0xFFBB86FC);

class ApplicationTheme {
  static final ThemeData darkTheme = _buildTheme();

  static ThemeData _buildTheme() {
    return ThemeData(
      useMaterial3: true,
      colorScheme: const ColorScheme(
        brightness: Brightness.dark,
        primary: kPrimaryColor,
        onPrimary: Color(0xFFFFFFFF),
        secondary: Color(0xFFFF00FF),
        onSecondary: Color(0xFFFF88FF),
        error: Color(0xFFBA1A1A),
        onError: Colors.red,
        surface: Color(0xFFFFFBFF),
        onSurface: Color(0xFF201A18),
        surfaceTint: Colors.white,
      ),
      scaffoldBackgroundColor: const Color(0xFF151515),
      dialogBackgroundColor: const Color.fromARGB(255, 42, 41, 41),
      primaryColor: kPrimaryColor,
      primaryColorLight: kPrimaryColorLight,
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          foregroundColor: Colors.white,
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 40.0),
          backgroundColor: kPrimaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          textStyle: const TextStyle(
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      textTheme: const TextTheme(
        bodyMedium: TextStyle(color: Colors.white),
        // bodySmall: TextStyle(color: Colors.white),
        // displayLarge: TextStyle(color: Colors.white),
        // displayMedium: TextStyle(color: Colors.white),
        // displaySmall: TextStyle(color: Colors.white),
        // titleLarge: TextStyle(color: Colors.white),
        // titleMedium: TextStyle(color: Colors.white),
        // titleSmall: TextStyle(color: Colors.white),
        // labelLarge: TextStyle(color: Colors.white),
        // labelMedium: TextStyle(color: Colors.white),
        // labelSmall: TextStyle(color: Colors.white),
        // headlineLarge: TextStyle(color: Colors.white),
        // headlineMedium: TextStyle(color: Colors.white),
        // headlineSmall: TextStyle(color: Colors.white),
      ),
    );
  }
}
