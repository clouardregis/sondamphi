import 'package:flutter/material.dart';
import 'package:voxamphi/model/survey.dart';

class ResponseTile extends StatelessWidget {
  final Survey survey;

  const ResponseTile({super.key, required this.survey});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List<Widget>.generate(survey.features.length, (index) {
        if (survey.totalVotes > 0) {
          return ResponseLine(survey: survey, index: index);
        } else {
          return Container();
        }
      }).toList(),
    );
  }
}

class ResponseLine extends StatelessWidget {
  final Survey survey;
  final int index;

  const ResponseLine({super.key, required this.survey, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          Text(
            survey.features[index].name,
            style: TextStyle(
              color: survey.features[index].color,
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: (survey.percent(index) * 100).floor(),
            child: Container(
              padding: const EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: survey.features[index].color,
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ),
          Expanded(
            flex: 100 - (survey.percent(index) * 100).floor(),
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                "${_doubleToString(survey.percent(index) * 100)} (${survey.voteCount(index)})",
                style: TextStyle(
                  color: survey.features[index].color,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _doubleToString(double value) {
    if (value == value.toInt()) {
      return value.toInt().toString();
    } else {
      return value.toStringAsFixed(1);
    }
  }
}
