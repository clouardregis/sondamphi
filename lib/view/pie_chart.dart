import 'dart:math';

import 'package:flutter/material.dart';
import 'package:voxamphi/model/survey.dart';

class NeumorphicPie extends StatelessWidget {
  final Survey survey;
  final List<double> radians = [];
  final List<double> percents = [];
  final double size;
  final double insideSize;

  NeumorphicPie({super.key, required this.survey, required this.size, required this.insideSize}) {
    assert(insideSize < size);
    double radian = 0;
    radians.add(radian);
    for (var i = 0; i < survey.totalResponses; ++i) {
      radian += 2 * pi * survey.percent(i);
      radians.add(radian);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 2 * size + insideSize,
        width: size + insideSize,
        child: Stack(
          children: [
            for (var i = 0; i < survey.features.length; ++i)
              CustomPaint(
                painter: ProgressRings(
                  completedPercentage: survey.percent(i),
                  circleWidth: size,
                  insideSize: insideSize,
                  color: survey.features[i].color,
                  progressStartAngle: radians[i],
                ),
                child: const Center(),
              ),
          ],
        ),
      ),
    );
  }
}

class ProgressRings extends CustomPainter {
  final double completedPercentage;
  final double circleWidth;
  final Color color;
  final double gradientStartAngle = 3 * pi / 2;
  final double gradientEndAngle = 4 * pi / 2;
  final double progressStartAngle;
  final double insideSize;

  ProgressRings({
    required this.completedPercentage,
    required this.color,
    required this.circleWidth,
    required this.insideSize,
    this.progressStartAngle = 0,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Offset center = Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    double arcAngle = 2 * pi * (completedPercentage);
    Rect boundingSquare = Rect.fromCircle(center: center, radius: radius);

    Paint paint(List<Color> colors, {double startAngle = 0.0, double endAngle = pi * 2}) {
      final Gradient gradient = SweepGradient(
        startAngle: startAngle,
        endAngle: endAngle,
        colors: colors,
      );
      return Paint()
        ..strokeCap = StrokeCap.butt
        ..style = PaintingStyle.stroke
        ..strokeWidth = circleWidth - insideSize
        ..shader = gradient.createShader(boundingSquare);
    }

    canvas.drawArc(boundingSquare, -pi / 2 + progressStartAngle, arcAngle, false, paint([color, color], startAngle: 0.0, endAngle: pi / 3));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
