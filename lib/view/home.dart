import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:voxamphi/model/survey.dart';
import 'package:voxamphi/presenter/presenter.dart';

import 'i18n.dart';
import 'pie_chart.dart';
import 'response_tile.dart';
import 'theme.dart';

class Home extends StatefulWidget {
  final Presenter presenter;

  const Home({super.key, required this.presenter});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future<Survey?>? _survey;
  late final ReceivePort _receivePort;
  double _progressValue = 0.0;
  late final Future<String> _serverIpAddress;
  late final ValueNotifier<bool> _isRunning;

  @override
  void initState() {
    _survey = Future.value(null);
    _isRunning = ValueNotifier(false);
    _serverIpAddress = widget.presenter.getServerIp();
    _receivePort = ReceivePort()
      ..listen((dynamic message) {
        setState(() {
          _progressValue = message;
        });
      });
    super.initState();
  }

  @override
  void dispose() {
    _receivePort.close();
    _isRunning.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 20.0, bottom: 10),
              child: TitleWidget(),
            ),
            Expanded(
              child: FutureBuilder<Survey?>(
                future: _survey,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: LinearProgressIndicator(
                          backgroundColor: kSecondaryColorLight,
                          valueColor: const AlwaysStoppedAnimation(kPrimaryColor),
                          minHeight: 16,
                          value: _progressValue,
                        ),
                      ),
                    );
                  } else {
                    if (snapshot.hasError) {
                      return Center(
                          child: Text(
                        I18n.getString("home_internal_error"),
                        textAlign: TextAlign.center,
                      ));
                    } else if (snapshot.hasData && snapshot.data != null) {
                      Survey data = snapshot.data!;
                      final int totalVotes = data.totalVotes;
                      return Column(
                        children: [
                          NeumorphicPie(
                            survey: data,
                            size: 100,
                            insideSize: 40,
                          ),
                          Center(
                            child: Text(
                              "$totalVotes ${totalVotes > 1 ? I18n.getString("survey_display_responses") : I18n.getString("survey_display_response")}",
                              style: const TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w300,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          ResponseTile(survey: data),
                        ],
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Image.asset(
                          'assets/images/logo.png',
                          isAntiAlias: true,
                          width: MediaQuery.of(context).size.width / 2,
                        ),
                      );
                    }
                  }
                },
              ),
            ),
            AnimatedBuilder(
                animation: _isRunning,
                builder: (context, snapshot) {
                  return SurveyButton(
                      onPressed: (_isRunning.value)
                          ? null
                          : () {
                              _survey = widget.presenter.processImage(_survey, _receivePort.sendPort);
                              _survey?.then((value) => _isRunning.value = false);
                              _isRunning.value = true;
                            });
                }),
            FutureBuilder<String>(
              future: _serverIpAddress,
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return IpWidget(
                      ip: I18n.getString(snapshot.error.toString()),
                      documentation: I18n.getString("ip_retrieving_error"),
                    );
                  } else {
                    return IpWidget(
                      ip: snapshot.data!,
                      documentation: I18n.getString("ip_retrieving_message"),
                    );
                  }
                }
                return const Center(child: Text("Connection not done"));
              },
            ),
          ],
        ),
      ),
    );
  }
}

class SurveyButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const SurveyButton({
    super.key,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
      width: double.infinity,
      child: TextButton(
        onPressed: onPressed,
        child: Text(
          onPressed == null ? I18n.getString("home_wait_button_label") : I18n.getString("home_survey_button_label"),
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}

class TitleWidget extends StatelessWidget {
  const TitleWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          I18n.getString("home_title"),
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w900,
          ),
        ),
        Text(
          I18n.getString("home_authors"),
          style: const TextStyle(
            fontSize: 9,
            fontStyle: FontStyle.italic,
          ),
        ),
      ],
    );
  }
}

class IpWidget extends StatelessWidget {
  final String _ip;
  final String _documentation;

  const IpWidget({super.key, required String ip, required String documentation})
      : _ip = ip,
        _documentation = documentation;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          _ip,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w300,
            color: Colors.grey,
          ),
        ),
        const SizedBox(width: 10),
        InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DialogInfoIpWidget(documentationText: _documentation);
                });
          },
          child: const Icon(Icons.info, size: 30, color: Colors.grey),
        ),
      ],
    );
  }
}

class DialogInfoIpWidget extends StatelessWidget {
  final String documentationText;

  const DialogInfoIpWidget({super.key, required this.documentationText});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              documentationText,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w300,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
