import 'dart:convert';
import 'dart:isolate';

import 'package:image_picker/image_picker.dart';
import 'package:voxamphi/model/http_server.dart';
import 'package:voxamphi/model/image_processor.dart';
import 'package:voxamphi/model/survey.dart';

class Presenter {
  late final VoxHTTPServer _server;

  Presenter() {
    _server = VoxHTTPServer(8080);
    _server.jsonResults = jsonEncode({"status": 404});
  }

  Future<void> startServer() async {
    _server.run();
  }

  Future<Survey?> processImage(Future<Survey?>? survey, SendPort sendToUiPort) async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.camera);
    if (image == null) {
      return survey;
    } else {
      Future<Survey> computation = Isolate.run<Survey>(ImageProcessor(imageFile: image, sendPort: sendToUiPort).analyse);
      computation.then((Survey result) {
        _server.jsonResults = jsonEncode({"status": 200, "data": result.asSerializable});
      });
      return computation;
    }
  }

  Future<String> getServerIp() async => await _server.getIpServer();
}
